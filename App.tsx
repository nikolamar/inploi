import * as React from 'react'
import RootNavigator from './src/RootNavigator'
import { Provider } from 'mobx-react'
import store from './src/store/Store'
const { Font } = require('expo')

class App extends React.Component<any> {
  state = {
    fontLoaded: false
  }
  async componentDidMount() {
    try {
      await Font.loadAsync({
        'avenir-next-bold': require("./assets/fonts/avenir-next-bold.ttf"),
        'avenir-next-italic': require("./assets/fonts/avenir-next-italic.ttf"),
        'avenir-next-regular': require("./assets/fonts/avenir-next-regular.ttf"),
        'avenir-next-thin': require("./assets/fonts/avenir-next-thin.ttf")
      })
    } catch(error) {
      console.log(error)
    }
    this.setState({ fontLoaded: true })
  }
  render() {
    if (!this.state.fontLoaded) return null

    return (
      <Provider store={store}>
        { <RootNavigator/>}
      </Provider>
    )
  }
}

export default App
