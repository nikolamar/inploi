import * as React from 'react'
import { inject, observer } from 'mobx-react'
import { StyleSheet, View } from 'react-native'
import { ListItem } from '../../basic-components'
import colors from '../../colors'
import { Store } from '../../models'

interface Props {
  store: Store
}

@inject('store') @observer
class Network extends React.Component<Props, any> {
  render() {
    return (
      <View style={styles.container}>
        <View>
          {this.props.store.fake.speciality.peopleList.map((item, i) => (
            <ListItem
              key={i}
              rightArrow
              title={item.title}
              subtitle={item.subtitle}
              leftAvatar={
                <View style={{
                  width: 38,
                  height: 38,
                  borderRadius: 3,
                  backgroundColor: colors.secondary2
                }}/>
              }
            />
          ))}
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    backgroundColor: colors.content
  }
})

export default Network