import * as React from 'react'
import { inject, observer } from 'mobx-react'
import { StyleSheet, ScrollView, View, Text } from 'react-native'
import { ListItem, Line, Grid, DescriptiveCard }  from '../../basic-components'
import { resetAction } from '../../nav-actions'
import colors from '../../colors'

interface Job {
  wage: string
  title: string
  description: string
  subtitle: string
}

interface Navigation {
  jumpToIndex: (i: number) => void,
  dispatch: any
  navigationState: {
    routes: {
      length: number
    },
    index: number
  }
}

interface Props {
  navigation: Navigation
  store: {
    fake: {
      speciality: {
        companiesList: Array<any>
      }
    }
  }
}

@inject('store') @observer
class Companies extends React.Component<Props, any> {
  renderFeaturedJob = (item: Job) => (
    <DescriptiveCard
      itemCardWidth={280}
      itemCardHeight={155}
      head={item.wage}
      title={item.title}
      titleColor={colors.primary2}
      subtitle={item.description}
      onPress={() => this.props.navigation.dispatch(resetAction('JobRoute'))}
    />
  )
  renderRecomendedJob = (item: Job) => (
    <DescriptiveCard
      head={item.wage}
      title={item.title}
      titleColor={colors.primary2}
      subtitle={item.company}
      onPress={() => this.props.navigation.dispatch(resetAction('JobRoute'))}
    />
  )
  render() {
    return (
      <ScrollView style={styles.container}>
        <Text style={[styles.bold, {marginBottom: 30}]}>
          Featured.{' '}
          <Text style={[styles.bold, {color: colors.secondary}]}>
            London’s top employers. 🏰
          </Text>
        </Text>
        <Grid
          horizontal
          itemWidth={300}
          itemHeight={224}
          data={this.props.store.fake.speciality.featuredCompanies}
          renderItem={this.renderFeaturedJob}
        />
        <Line style={{marginVertical: 30}}/>
        <Text style={[styles.bold, {marginBottom: 30}]}>
          Following.{' '}
          <Text style={[styles.bold, {color: colors.secondary}]}>
          Your Companies. ⚡
          </Text>
        </Text>
        <Grid
          horizontal
          itemWidth={145}
          itemHeight={169}
          data={this.props.store.fake.speciality.recomendedCompaniesJobs}
          renderItem={this.renderRecomendedJob}
        />
        <Line style={{marginVertical: 30}}/>
        <Text style={[styles.bold, {marginBottom: 20}]}>All Companies</Text>
        <View>
          {this.props.store.fake.speciality.companiesList.map((item: Job, i: number) => (
            <ListItem
              key={i}
              rightArrow
              title={item.title}
              subtitle={item.subtitle}
              leftAvatar={
                <View style={{
                  width: 38,
                  height: 38,
                  borderRadius: 3,
                  backgroundColor: colors.secondary2
                }}/>
              }
            />
          ))}
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  bold: {
    fontWeight: 'bold',
    fontFamily: 'avenir-next-bold'
  },
  regular: {
    fontFamily: 'avenir-next-regular'
  },
  container: {
    flex: 1,
    marginTop: 30,
    paddingHorizontal: 20,
    backgroundColor: colors.content
  }
})

export default Companies