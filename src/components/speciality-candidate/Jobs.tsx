import * as React from 'react'
import { inject, observer } from 'mobx-react'
import { StyleSheet, ScrollView, View, Text } from 'react-native'
import { ListItem, Line, Grid, DescriptiveCard }  from '../../basic-components'
import { resetAction } from '../../nav-actions'
import colors from '../../colors'
import { Navigation, Store } from '../../models'

interface Props {
  store: Store
  navigation: Navigation
}

interface Job {
  wage: string
  description: string
  title: string
  company: string
}

@inject('store') @observer
class Jobs extends React.Component<Props, any> {
  renderFeaturedJob = (item: Job) => (
    <DescriptiveCard
      itemCardWidth={280}
      itemCardHeight={155}
      head={item.wage}
      title={item.title}
      titleColor={colors.primary2}
      subtitle={item.description}
      onPress={() => this.props.navigation.dispatch(resetAction('JobRoute'))}
    />
  )
  renderRecomendedJob = (item: Job) => (
    <DescriptiveCard
      head={item.wage}
      title={item.title}
      subtitle={item.company}
      onPress={() => this.props.navigation.dispatch(resetAction('JobRoute'))}
    />
  )
  render() {
    return (
      <ScrollView style={styles.container}>
        <Text style={[styles.bold, {marginBottom: 30}]}>
          Featured.{' '}
          <Text style={[styles.bold, {color: colors.secondary}]}>
            The hottest, freshest jobs around. 😎
          </Text>
        </Text>
        <Grid
          horizontal
          itemWidth={300}
          itemHeight={224}
          data={this.props.store.fake.speciality.featuredJobs}
          renderItem={this.renderFeaturedJob}
        />
        <Line style={{marginVertical: 30}}/>
        <Text style={[styles.bold, {marginBottom: 30}]}>
          For you.{' '}
          <Text style={[styles.bold, {color: colors.secondary}]}>
            Recommended by us. 🔬
          </Text>
        </Text>
        <Grid
          horizontal
          itemWidth={145}
          itemHeight={169}
          data={this.props.store.fake.speciality.recomendedJobsJobs}
          renderItem={this.renderRecomendedJob}
        />
        <Line style={{marginVertical: 30}}/>
        <Text style={[styles.bold, {marginBottom: 20}]}>All Jobs</Text>
        <View>
          {this.props.store.fake.speciality.jobsList.map((item: any, i: number) => (
            <ListItem
              key={i}
              rightArrow
              title={item.title}
              subtitle={item.subtitle}
              leftAvatar={
                <View style={{
                  width: 38,
                  height: 38,
                  borderRadius: 3,
                  backgroundColor: colors.secondary2
                }}/>
              }
            />
          ))}
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  bold: {
    fontWeight: 'bold',
    fontFamily: 'avenir-next-bold'
  },
  regular: {
    fontFamily: 'avenir-next-regular'
  },
  container: {
    flex: 1,
    marginTop: 30,
    paddingHorizontal: 20,
    backgroundColor: colors.content
  }
})

export default Jobs