import * as React from 'react'
import { StackNavigator, TabNavigator } from 'react-navigation'
import { StyleSheet, Text } from 'react-native'
import JobListing from './JobListing'
import { ClassicTabs } from '../../basic-components'
import Jobs from './Jobs'
import Companies from './Companies'
import Network from './Network'
import colors from '../../colors'
import { Route } from '../../enums'

interface Props {
  navigation: object
}

const styles = StyleSheet.create({
  text: {fontFamily: 'AvenirNext-Bold', fontSize: 30, paddingLeft: 15}
})

const Tabs = {
  screen: TabNavigator({
    [Route.Jobs]: {
      screen: Jobs,
      navigationOptions: ({navigation}: Props) => ({
        title: 'Jobs',
      })
    },
    [Route.Companies]: {
      screen: Companies,
      navigationOptions: ({navigation}: Props) => ({
        title: 'Companies',
      })
    },
    [Route.Network]: {
      screen: Network,
      navigationOptions: ({navigation}: Props) => ({
        title: 'Network',
      })
    }
  },
  {
    swipeEnabled: true,
    animationEnabled: true,
    tabBarComponent: navigation => (
      <ClassicTabs navigation={navigation}/>
    ),
    tabBarPosition: 'top'
  })
}

const SpecialityCandidate = {
  screen: StackNavigator(
    {
      screen: { screen: JobListing }
    },
    {
      navigationOptions: {
        headerTitle: '',
        headerLeft: (
          <Text style={styles.text}>Discover</Text>
        ),
        headerStyle: {
          backgroundColor: colors.content,
          borderBottomWidth: 0,
          marginTop: 15,
          elevation: 0,       //remove shadow on Android
          shadowOpacity: 0,   //remove shadow on iOS
        }
      },
      cardStyle: {
        backgroundColor: colors.content
      }
    }
  )
}

export default Tabs