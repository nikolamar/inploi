import * as React from 'react'
import { StyleSheet, View, Text } from 'react-native'
import { Button } from '../../../basic-components'
import colors from '../../../colors'

const JobFooter = () => (
  <View style={styles.container}>
    <Text style={styles.text}>
      Want to know more about this company? Check out their company page for more jobs, company updates news and more.
    </Text>
    <Button
      color={colors.content}
      inverted
      style={styles.button}
    >
      View Company Page
    </Button>
  </View>
)

const styles = StyleSheet.create({
  container: {
    height: 181,
    backgroundColor: colors.primary4,
    padding: 20
  },
  text: {
    color: colors.title,
    fontSize: 15
  },
  button: {
    alignSelf: "flex-end",
    width: 195,
    marginTop: 20
  }
})

export default JobFooter