import * as React from 'react'
import { inject, observer } from 'mobx-react'
import { StyleSheet, View, Text } from 'react-native'
import colors from '../../../colors'
const { MapView } = require('expo')
const { MaterialIcons } = require('@expo/vector-icons')

interface Props {
  style: object
  store: {
    fake: {
      job: {
        address: string
        latitude: number
        longitude: number
      }
    }
  }
}

@inject('store') @observer
class JobLocation extends React.Component<Props, any> {
  render() {
    return (
      <View style={this.props.style}>
        <View style={{
          height: 55,
          flexDirection: 'row',
          paddingHorizontal: 20,
          backgroundColor: colors.primary3,
          alignItems: 'center'
        }}>
          <Text style={styles.address}>
            {this.props.store.fake.job.address}
          </Text>
          <MaterialIcons
            name='keyboard-arrow-right'
            size={35}
            color={colors.content}
          />
        </View>
        <MapView
          style={{height: 200}}
          initialRegion={{
            latitude: this.props.store.fake.job.latitude,
            longitude: this.props.store.fake.job.longitude,
            latitudeDelta: 0.005,
            longitudeDelta: 0.005,
          }}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  address: {
    flex: 1,
    fontSize: 15,
    color: colors.title
  }
})

export default JobLocation