import * as React from 'react'
import { inject, observer } from 'mobx-react'
import { StyleSheet, View, Text } from 'react-native'
import { Grid, DescriptiveCard } from '../../../basic-components'

interface Props {
  style: object
  store: {
    fake: {
      speciality: {
        similarJobs: object
      }
    }
  }
}

interface Job {
  title: string
  company: string
}

const renderSimilarJob = (item: Job) => (
  <DescriptiveCard
    textContainer={styles.textContainer}
    title={item.title}
    subtitle={item.company}
  />
)

@inject('store') @observer
class SimilarJobs extends React.Component<Props, any> {  
  render() {
    return (
      <View style={this.props.style}>
        <Text style={styles.text}>Similar Jobs</Text>
        <Grid
          horizontal
          itemWidth={145}
          itemHeight={169}
          data={this.props.store.fake.speciality.similarJobs}
          renderItem={renderSimilarJob}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  textContainer: {alignItems: 'center'},
  text: {fontWeight: 'bold', fontSize: 15}
})

export default SimilarJobs