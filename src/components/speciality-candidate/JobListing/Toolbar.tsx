import * as React from 'react'
import { TouchableOpacity, View, Text } from 'react-native'
import { Line } from '../../../basic-components'
import { resetAction } from '../../../nav-actions'
const { MaterialIcons } = require('@expo/vector-icons')

interface Props {
  navigation: any
}

const Toolbar = ({navigation}: Props) => (
  <View>
    <View style={{flexDirection: "row", alignItems: "center", height: 60}}>
      <MaterialIcons
        name="keyboard-arrow-left"
        size={35}
      />
      <TouchableOpacity
        style={{flex: 1}}
        onPress={() => navigation.dispatch(resetAction("HomeRoute"))}
      >
        <Text style={{fontWeight: "600"}}>Parent Title</Text>
      </TouchableOpacity>
      <MaterialIcons
        name="share"
        size={25}
      />
      <MaterialIcons
        style={{margin: 10}}
        name="favorite-border"
        size={25}
      />
    </View>
    <Line/>
  </View>
)

export default Toolbar