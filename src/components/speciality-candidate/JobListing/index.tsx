import * as React from 'react'
import { StyleSheet, ScrollView, View, Text } from 'react-native'
import { Line } from '../../../basic-components'
import Toolbar from './Toolbar'
import SimilarJobs from './SimilarJobs'
import JobFooter from './JobFooter'
import JobLocation from './JobLocation'
import JobDetails from './JobDetails'
import AboutJob from './AboutJob'
import Slider from './Slider'

interface Props {
  navigation: object
}

const JobListing = ({navigation}: Props) => (
  <View style={{marginTop: 20}}>
    <Toolbar navigation={navigation}/>
    <ScrollView>
      <AboutJob style={styles.about}/>
      <Line style={styles.line}/>
      <Slider style={styles.slider}/>
      <Line style={styles.line}/>
      <Text style={styles.text}>DETAILS</Text>
      <JobDetails style={styles.details}/>
      <JobLocation/>
      <SimilarJobs style={styles.similar}/>
      <JobFooter/>
    </ScrollView>
  </View>
)

const styles = StyleSheet.create({
  about: { marginHorizontal: 20 },
  line: { marginVertical: 30, marginHorizontal: 20 },
  slider: { marginHorizontal: 20, height: 186 },
  text: { fontWeight: 'bold', marginLeft: 20 },
  details: { marginHorizontal: 20 },
  similar: {marginHorizontal: 20, marginVertical: 30}
})

export default JobListing