import * as React from 'react'
import { inject, observer } from 'mobx-react'
import { View } from 'react-native'
import { ListItem } from '../../../basic-components'
import colors from '../../../colors'
const { MaterialIcons } = require('@expo/vector-icons')

interface Props { 
  style: object
  store: {
    fake: {
      job: {
        pay: string
        category: string
        required: string
        skills: string
      }
    }
  }
}

@inject('store') @observer
class JobDetails extends React.Component<Props, any> {
  render() {
    return (
      <View style={this.props.style}>
        <ListItem
          subtitle={this.props.store.fake.job.category}
          leftAvatar={
            <MaterialIcons
              name="loyalty"
              size={38}
              color={colors.secondary2}
            />
          }
        />
        <ListItem
          subtitle={this.props.store.fake.job.pay}
          leftAvatar={
            <MaterialIcons
              name="slow-motion-video"
              size={38}
              color={colors.secondary2}
            />
          }
        />
        <ListItem
          subtitle={this.props.store.fake.job.required}
          leftAvatar={
            <MaterialIcons
              name="account-balance-wallet"
              size={38}
              color={colors.secondary2}
            />
          }
        />
        <ListItem
          subtitle={this.props.store.fake.job.skills}
          leftAvatar={
            <MaterialIcons
              name="assessment"
              size={38}
              color={colors.secondary2}
            />
          }
        />
      </View>
    )
  }
}

export default JobDetails