import * as React from 'react'
import { StackNavigator } from 'react-navigation'
import { StyleSheet, TouchableOpacity, Text } from 'react-native'
import Splash from './Splash'
import Split from './Split'
import CreateAccount from './CreateAccount'
import Login from './Login'
import ForgotPassword from './ForgotPassword'
import colors from '../../colors'
import { Route } from '../../enums'
import { Navigation } from '../../models'
const callOnceInInterval = require('call-once-in-interval')

interface Props {
  navigation: Navigation
}

export default { 
  screen: StackNavigator(
    {
      [Route.Split]: {
        screen: Split,
        navigationOptions: ({navigation}: Props) => ({
          title: 'Split',
          headerRight: <TouchableOpacity onPress={callOnceInInterval(() => navigation.navigate(Route.Login), 1000)}>
            <Text style={styles.headerRight}>Login</Text>
          </TouchableOpacity>
        })
      },
      [Route.CreateAccount]: {
        screen: CreateAccount,
        navigationOptions: ({navigation}: Props) => ({
          title: 'CreateAccount',
          headerRight: <TouchableOpacity onPress={callOnceInInterval(() => navigation.navigate(Route.Login), 1000)}>
            <Text style={styles.headerRight}>Login</Text>
          </TouchableOpacity>
        })
      },
      [Route.Login]: {
        screen: Login,
        navigationOptions: ({navigation}: Props) => ({
          title: 'Login',
          headerRight: <TouchableOpacity onPress={callOnceInInterval(() => navigation.navigate(Route.ForgotPassword), 1000)}>
            <Text style={styles.headerRight}>Forgot Password</Text>
          </TouchableOpacity>
        })
      },
      [Route.ForgotPassword]: {
        screen: ForgotPassword,
        navigationOptions: ({navigation}: Props) => ({
          title: 'ForgotPassword'
        })
      },
      [Route.Splash]: {
        screen: Splash,
        navigationOptions: ({navigation}: Props) => ({
          title: 'Splash',
        })
      }
    },
    {
      navigationOptions: {
        headerTintColor: colors.secondary,
        headerTitle: ' ',
        headerStyle: {
          backgroundColor: colors.content,
          borderBottomWidth: 0,
          marginTop: 15,
          elevation: 0,       //remove shadow on Android
          shadowOpacity: 0,   //remove shadow on iOS
        }
      },
      cardStyle: {
        backgroundColor: colors.content
      }
    }
  )
}

const styles = StyleSheet.create({
  headerRight: {
    marginRight: 20
  }
})