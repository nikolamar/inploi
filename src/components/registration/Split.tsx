import * as React from 'react'
import { StyleSheet, View, Text } from 'react-native'
import { Button } from '../../basic-components'
import colors from '../../colors'
import { Route } from '../../enums'

interface Props {
  navigation: {
    navigate: (name: string) => void
  }
}

const Split = ({navigation}: Props) => (
  <View style={styles.container}>
    <Text style={styles.title}>
      Welcome
    </Text>
    <Text style={styles.subTitle}>
      Create an account to get started!
    </Text>
    <View style={styles.space}/>
    <Button
      style={styles.button}
      color={colors.primary}
      onPress={() => navigation.navigate(Route.CreateAccount)}
    >
      Find a Job
    </Button>
    <Button
      inverted
      style={styles.button}
      color={colors.primary}
    >
      Recruit Top Stuff
    </Button>
    <Text style={styles.signupContainer}>
      By signing up, I agree to inploi's{' '}
      <Text style={styles.signupPrimary}>
        Terms{' '}
      </Text>
      <Text>
        and{' '}
      </Text>
      <Text style={styles.signupPrimary}>
        Privacy Policy.
      </Text>
    </Text>
  </View>
)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20
  },
  title: {
    fontSize: 30,
    marginBottom: 30,
    fontWeight: 'bold',
    color: colors.secondary,
    fontFamily: 'avenir-next-bold'
  },
  subTitle: {
    fontSize: 15,
    fontFamily: 'avenir-next-regular',
    color: colors.secondary
  },
  signupContainer: {
    marginVertical: 30,
    fontSize: 12,
    fontFamily: 'avenir-next-regular',
    textAlign: 'center'
  },
  signupPrimary: {
    fontSize: 12,
    fontFamily: 'avenir-next-regular',
    color: colors.primary
  },
  button: {
    height: 50,
    marginVertical: 8
  },
  containerText: {
    flex: 1
  },
  space: {
    flex: 1
  }
})

export default Split