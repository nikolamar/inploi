import * as React from 'react'
import { StyleSheet, View, Text, TextInput } from 'react-native'
import { Button } from '../../basic-components'
import colors from '../../colors'

interface Props {}

interface State {
  email: string
}

class Split extends React.Component<Props, State> {
  emailInput: any
  state = {
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    phoneNumber: ''
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>
          Forgot Password
        </Text>
        <Text style={styles.subTitle}>
          We will send an email to alex@inploi.me. Tap the link in the email to reset your password.
        </Text>
        <TextInput
          placeholder='Email Address'
          autoCapitalize='none'
          autoCorrect={false}
          style={styles.input}
          onChangeText={email => this.setState({email})}
          value={this.state.email}
          keyboardType='email-address'
          returnKeyType='go'
          ref={ref => this.emailInput = ref}
        />
        <View style={styles.space}/>
        <Button
          style={styles.button}
          color={colors.primary}
        >
          Reset Password
        </Button>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20
  },
  title: {
    fontSize: 30,
    marginBottom: 30,
    color: colors.secondary,
    fontFamily: 'avenir-next-bold'
  },
  subTitle: {
    fontSize: 15,
    marginBottom: 30,
    fontFamily: 'avenir-next-regular',
    color: colors.secondary
  },
  button: {
    height: 50,
    marginVertical: 8
  },
  input: {
    height: 50,
    fontSize: 15,
    paddingHorizontal: 10,
    borderRightWidth: 0,
    borderTopWidth: 0,
    borderBottomWidth: 1,
    borderTopRightRadius: 3,
    borderBottomRightRadius: 3,
    borderColor: colors.secondary2,
    fontFamily: 'avenir-next-regular',
  },
  space: {
    flex: 1
  }
})

export default Split