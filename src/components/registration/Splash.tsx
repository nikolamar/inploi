import * as React from 'react'
import { StyleSheet, View, Image } from 'react-native'

const Splash = () => (
  <View style={styles.container}>
    <Image
      style={styles.logo}
      resizeMode='contain'
      source={require('../../../assets/images/inploi-logo-red.png')}
    />
  </View>
)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  logo: {
    width: 160,
    height: 76
  }
})

export default Splash