import * as React from 'react'
import { StyleSheet, KeyboardAvoidingView, View, Text, TextInput } from 'react-native'
import { Button } from '../../basic-components'
import colors from '../../colors'
import { Navigation } from '../../models'
import { Route } from '../../enums'
const callOnceInInterval = require('call-once-in-interval')

interface Props {
  navigation: Navigation
}

interface State {
  email: string
  password: string
}

class Split extends React.Component<Props, State> {
  emailInput: any
  passwordInput: any
  state = {
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    phoneNumber: ''
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>
          Login
        </Text>
        <TextInput
          placeholder='Email Address'
          autoCapitalize='none'
          autoCorrect={false}
          style={styles.input}
          onChangeText={email => this.setState({email})}
          value={this.state.email}
          keyboardType='email-address'
          returnKeyType='next'
          ref={ref => this.emailInput = ref}
          onSubmitEditing={() => this.passwordInput.focus()}
        />
        <TextInput
          placeholder='Password'
          autoCorrect={false}
          style={styles.input}
          onChangeText={password => this.setState({password})}
          value={this.state.password}
          returnKeyType='go'
          ref={ref => this.passwordInput = ref}
        />
        <View style={styles.space}/>
        <KeyboardAvoidingView behavior='padding'>
          <Button
            style={styles.button}
            color={colors.primary}
            onPress={callOnceInInterval(() => this.props.navigation.navigate(Route.CandidateWelcome), 1000)}
          >
            Login In
          </Button>
          <Button
            icon='linkedin'
            style={styles.button}
            color={colors.primary5}
          >
            Log In with LinkedIn
          </Button>
          <Button
            icon='facebook-f'
            style={styles.button}
            color={colors.primary6}
          >
            Log In with Facebook
          </Button>
        </KeyboardAvoidingView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20
  },
  title: {
    fontSize: 30,
    marginBottom: 30,
    fontWeight: 'bold',
    color: colors.secondary,
    fontFamily: 'avenir-next-bold'
  },
  button: {
    height: 50,
    marginVertical: 8
  },
  input: {
    height: 50,
    fontSize: 15,
    paddingHorizontal: 10,
    borderRightWidth: 0,
    borderTopWidth: 0,
    borderBottomWidth: 1,
    borderTopRightRadius: 3,
    borderBottomRightRadius: 3,
    borderColor: colors.secondary2,
    fontFamily: 'avenir-next-regular',
  },
  space: {
    flex: 1
  }
})

export default Split