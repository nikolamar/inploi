import * as React from 'react'
import { StyleSheet, View } from 'react-native'
import colors from '../colors'

interface Props {
  children: Element
}

const Feeds = ({children}: Props) => (
  <View style={styles.container}>
    {children}
  </View>
)

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.primary,
    borderRadius: 15,
    padding: 15
  }
})

export default Feeds