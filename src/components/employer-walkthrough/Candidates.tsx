import * as React from 'react'
import { StyleSheet, View, Image, Text } from 'react-native'
import colors from '../../colors'

const Candidates = () => (
  <View style={{flex: 1}}>
    <Image
      style={{
        flex: 1,
        width: undefined,
        height: undefined
      }}
      resizeMode='contain'
      source={require('../../../assets/images/walkthrough-employer-candidates.png')}
    />
    <View style={{padding: 15}}>
      <Text style={styles.title}>
        Recruit Talent
      </Text>
      <Text style={styles.text}>
        List jobs, create company pages, create connections, active recruitment.
      </Text>
    </View>
  </View>
)

const styles = StyleSheet.create({
  title: {
    marginTop: 50,
    fontSize: 30,
    color: colors.secondary,
    fontFamily: 'avenir-next-bold'
  },
  text: {
    marginVertical: 50,
    fontSize: 15,
    fontFamily: 'avenir-next-regular',
    color: colors.secondary
  }
})

export default Candidates