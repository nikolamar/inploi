import * as React from 'react'
import { TabNavigator } from 'react-navigation'
import Welcome from './Welcome'
import Candidates from './Candidates'
import Discover from './Discover'
import { TabsFooter } from '../../basic-components'
import { Navigation } from '../../models'
import { Route } from '../../enums'

interface Props {
  navigation: Navigation
}

export default { 
  screen: TabNavigator(
    {
      [Route.EmployerWelcome]: {
        screen: Welcome,
        navigationOptions: ({navigation}: Props) => ({
          title: 'Welcome',
        })
      },
      [Route.EmployerCandidates]: {
        screen: Candidates,
        navigationOptions: ({navigation}: Props) => ({
          title: 'Candidates',
        })
      },
      [Route.EmployerDiscover]: {
        screen: Discover,
        navigationOptions: ({navigation}: Props) => ({
          title: 'Discover',
        })
      },
    },
    {
      swipeEnabled: true,
      animationEnabled: true,
      tabBarComponent: (navigation: Navigation) => <TabsFooter navigation={navigation}/>
    }
  )
}