import * as React from 'react'
import { StyleSheet, View, Image, Text } from 'react-native'
import colors from '../../colors'

const Welcome = () => (
  <View style={{flex: 1}}>
    <Image
      style={{
        flex: 1,
        width: undefined,
        height: undefined
      }}
      resizeMode='contain'
      source={require('../../../assets/images/walkthrough-employer-welcome.png')}
    />
    <View style={{padding: 15}}>
      <Text style={style.title}>
        Welcome
      </Text>
      <Text style={style.text}>
        This is where we have a really clear candidate mission statement. Best jobs in London and the rest of it.
      </Text>
    </View>
  </View>
)

const style = StyleSheet.create({
  title: {
    marginTop: 50,
    fontSize: 30,
    color: colors.secondary,
    fontFamily: 'avenir-next-bold'
  },
  text: {
    marginVertical: 50,
    fontSize: 15,
    color: colors.secondary,
    fontFamily: 'avenir-next-regular',
  }
})

export default Welcome