import * as React from 'react'
import { StyleSheet, View, Image, Text } from 'react-native'
import colors from '../../colors'

const Discover = () => (
  <View style={{flex: 1}}>
    <Image
      style={{
        flex: 1,
        width: undefined,
        height: undefined
      }}
      resizeMode='contain'
      source={require('../../../assets/images/walkthrough-employer-candidates.png')}
    />
    <View style={{padding: 15}}>
      <Text style={style.title}>
        Connect
      </Text>
      <Text style={style.text}>
        Chat with employers, introduce yourself with a video, join virtual interviews and schedule meetings.
      </Text>
    </View>
  </View>
)

const style = StyleSheet.create({
  title: {
    marginTop: 50,
    fontSize: 30,
    color: colors.secondary,
    fontFamily: 'avenir-next-bold'
  },
  text: {
    marginVertical: 50,
    fontSize: 15,
    fontFamily: 'avenir-next-regular',
    color: colors.secondary
  }
})

export default Discover