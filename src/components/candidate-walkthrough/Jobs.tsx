import * as React from 'react'
import { inject, observer } from 'mobx-react'
import { StyleSheet, View, Text } from 'react-native'
import { Grid, Card } from '../../basic-components'
import colors from '../../colors'

const renderItem = (item: object) => (
  <Card label={item.title}/>
)

@inject('store') @observer
class Jobs extends React.Component<any> {
  render() {
    return (
      <View style={{flex: 1, padding: 15}}>
        <Grid
          itemWidth={100}
          itemHeight={100}
          marginVertical={10}
          data={this.props.store.fake.speciality.jobsList}
          renderItem={renderItem}
        />
        <Text style={style.title}>
          Discover Jobs
        </Text>
        <Text style={style.text}>
          Get updates when jobs are added. Tap some titles above to get started. You can fine tune this later.
        </Text>
      </View>
    )
  }
}

const style = StyleSheet.create({
  title: {
    fontSize: 30,
    color: colors.secondary,
    fontFamily: 'avenir-next-bold'
  },
  text: {
    marginVertical: 50,
    fontSize: 15,
    fontFamily: 'avenir-next-regular',
    color: colors.secondary
  }
})

export default Jobs