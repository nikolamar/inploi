import * as React from 'react'
import { TabNavigator } from 'react-navigation'
import Welcome from './Welcome'
import Jobs from './Jobs'
import Discover from './Discover'
import { TabsFooter } from '../../basic-components'
import { Navigation } from '../../models'
import { Route } from '../../enums'

interface Props {
  navigation: Navigation
}

export default { 
  screen: TabNavigator(
    {
      [Route.CandidateWelcome]: {
        screen: Welcome,
        navigationOptions: ({navigation}: Props) => ({
          title: 'Welcome'
        })
      },
      [Route.CandidateJobs]: {
        screen: Jobs,
        navigationOptions: ({navigation}: Props) => ({
          title: 'Jobs'
        })
      },
      [Route.CandidateDiscover]: {
        screen: Discover,
        navigationOptions: ({navigation}: Props) => ({
          title: 'Discover'
        })
      }
    },
    {
      swipeEnabled: true,
      animationEnabled: true,
      tabBarComponent: (navigation: Navigation) => <TabsFooter navigation={navigation}/>
    }
  )
}