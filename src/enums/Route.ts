enum Route {
  // Registration
  Split = 'Split',
  CreateAccount = 'CreateAccount',
  Login = 'Login',
  ForgotPassword = 'ForgotPassword',
  Splash = 'Splash',

  // Candidate Walkthrough
  CandidateWelcome = 'CandidateWelcome',
  CandidateJobs = 'CandidateJobs',
  CandidateDiscover = 'CandidateDiscover',

  // Employer Walkthrough
  EmployerWelcome = 'EmployerWelcome',
  EmployerCandidates = 'EmployerCandidates',
  EmployerDiscover = 'EmployerDiscover',

  // Speciality Candidate
  Jobs = 'Jobs',
  Companies = 'Companies',
  Network = 'Network'
}

export default Route