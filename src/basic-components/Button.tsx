import * as React from 'react'
import { StyleSheet, View, TouchableOpacity, Text } from 'react-native'
import colors from '../colors'
const { FontAwesome } = require('@expo/vector-icons')

interface Props {
  onPress?: () => void
  inverted?: boolean
  children: string
  style?: {}
  color: string
  icon?: string
}

const Button = ({onPress, inverted, children, style, color, icon}: Props) => (
  <TouchableOpacity
    onPress={onPress}
    style={[
      styles.container,
      {borderColor: color,
      backgroundColor: inverted ? 'transparent' : color},
      style
    ]}
  >
    <View style={styles.subContainer}>
      <Text style={[styles.text, {color: inverted ? color : colors.content}]}>
        {children}
      </Text>
      {icon &&
      <View style={styles.iconContainer}>
        <FontAwesome
          style={styles.icon}
          name={icon}
          size={20}
          color={colors.secondary2}
        />
        <View style={styles.verticalLine}/>
      </View>}
    </View>
  </TouchableOpacity>
)

const styles = StyleSheet.create({
  container: {
    height: 40,
    borderRadius: 3,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  subContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  iconContainer: {
    width: 65,
    position: 'absolute',
    flexDirection: 'row',
    alignItems: 'center'
  },
  verticalLine: {
    height: 40,
    width: 1,
    backgroundColor: colors.content
  },
  text: {
    flex: 1,
    textAlign: 'center'
  },
  icon: {
    flex: 1,
    textAlign: 'center'
  }
})

export default Button