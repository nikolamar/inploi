import * as React from 'react'
import { StyleSheet, ScrollView, View } from 'react-native'
import { Event } from '../models'

interface Props {
  horizontal: boolean
  renderItem: (item: object, i: number) => Function
  data: Array<any>
  itemWidth: number
  itemHeight: number
  margin: number
  marginVertical: number
  style?: object
}

interface State {
  width: number
}

class GridView extends React.Component<Props, State> {
  state = {
    width: 0
  }
  renderItem = (item: object, i: number) => (
    <View
      key={i}
      style={{
        width: this.props.itemWidth,
        height: this.props.itemHeight,
        margin: this.props.margin,
        marginVertical: this.props.marginVertical
      }}
    >
      {this.props.renderItem(item, i)}
    </View>
  )
  renderEmptyViews = () => {
    if (!this.state.width) return
    let emptyViews = []
    const itemsInRow = Math.floor(this.state.width / this.props.itemWidth)
    const remainder = (this.props.data.length + 1) % itemsInRow
    for (let i = 0; i <= remainder; i++) {
      emptyViews.push(
        <View
          key={i}
          style={{
            width: this.props.itemWidth,
            height: this.props.itemHeight,
            margin: this.props.margin,
            marginVertical: this.props.marginVertical
          }}
        />
      )
    }
    return emptyViews
  }
  onLayout = (event: Event)  => {
    this.setState({ width: event.nativeEvent.layout.width })
  }
  render() {
    return (
      <ScrollView
        horizontal={this.props.horizontal}
        contentContainerStyle={[styles.content, this.props.style]}
        onLayout={this.onLayout}
      >
        {this.props.data.map((item, i) => this.renderItem(item, i))}
        {!this.props.horizontal ? this.renderEmptyViews() : null}
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  content: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
})

export default GridView