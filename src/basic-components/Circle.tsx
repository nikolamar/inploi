import * as React from 'react'
import { StyleSheet, View } from 'react-native'
import colors from '../colors'

interface Props {
  active: boolean
}

const Circle = ({active}: Props) => (
  <View style={[styles.circle, {opacity: active ? 1 : 0.3}]}/>
)

const styles = StyleSheet.create({
  circle: {
    width: 8,
    height: 8,
    marginLeft: 5,
    borderRadius: 8,
    backgroundColor: colors.primary
  }
})

export default Circle