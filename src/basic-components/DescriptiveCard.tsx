import * as React from 'react'
import { StyleSheet, TouchableOpacity, View, Text } from 'react-native'
import colors from '../colors'

interface Props {
  textContainer: object
  title: string
  subtitle: string
  head: boolean
  itemCardWidth: number
  itemCardHeight: number
  titleColor: string
  onPress: () => void
}

const DescriptiveCard = ({textContainer, title, subtitle, head, itemCardWidth = 125, itemCardHeight = 125, titleColor, onPress}: Props) => (
  <TouchableOpacity onPress={onPress} style={{width: itemCardWidth}}>
    <View style={[styles.card, {height: itemCardHeight}]}>
      {head && <View style={styles.cardHeaderContainer}>
        <Text numberOfLines={1} style={styles.cardHeader}>{head}</Text>
      </View>}
    </View>
    <View style={textContainer}>
      <Text style={[styles.cardTitle, {color: titleColor}]}>{title.toUpperCase()}</Text>
      <Text style={styles.cardSubtitle}>{subtitle}</Text>
    </View>
  </TouchableOpacity>
)

const styles = StyleSheet.create({
  card: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    backgroundColor: colors.secondary2,
    borderRadius: 6
  },
  cardHeaderContainer: {
    margin: 10,
    height: 25,
    paddingHorizontal: 5,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.content,
    borderRadius: 10,
    opacity: 0.8
  },
  cardHeader: {
    margin: 5,
    fontSize: 12,
    fontWeight: 'bold',
    fontFamily: 'avenir-next-bold'
  },
  cardTitle: {
    marginTop: 7,
    fontSize: 12,
    fontWeight: 'bold',
    fontFamily: 'avenir-next-bold',
    color: colors.primary2
  },
  cardSubtitle: {
    marginTop: 3,
    fontSize: 15,
    fontFamily: 'avenir-next-regular',
    color: colors.secondary
  }
})

export default DescriptiveCard