import * as React from 'react'
import { StyleSheet, TouchableOpacity, View, Text } from 'react-native'
import colors from '../colors'

interface Props {
  label: string,
  onPress: () => void
  width: number
  height: number
}

const Card = ({label, onPress, width = 100, height = 100}: Props) => (
  <TouchableOpacity
    onPress={onPress}
    style={[styles.container, {width, height}]}
  >
    <View style={styles.content}/>
    <View style={styles.labelContainer}>
      <Text numberOfLines={1} style={styles.label}>{label && label.toUpperCase()}</Text>
    </View>
  </TouchableOpacity>
)

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.secondary2,
    borderRadius: 6
  },
  label: {
    margin: 5,
    fontSize: 12,
    fontFamily: 'AvenirNext-Regular',
    fontWeight: '500',
    textAlign: 'center'
  },
  content: {
    flex: 1
  },
  labelContainer: {
    height: 25,
    backgroundColor: colors.content,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    opacity: 0.8
  }
})

export default Card