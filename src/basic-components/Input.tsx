import * as React from 'react'
import { StyleSheet, TouchableOpacity, View, TextInput } from 'react-native'
import colors from '../colors'
const { Ionicons } = require('@expo/vector-icons')

interface Props {
  style?: object
  onPressIcon?: () => void
  onChange?: () => void
  value?: any
  placeholder?: string
}

const Input = ({style, onPressIcon, onChange, value = '', placeholder = ''}: Props) => (
  <View style={[styles.container, style]}>
    <TouchableOpacity style={styles.icon} onPress={onPressIcon}>
      <Ionicons
        name='ios-search'
        size={30}
        color={colors.secondary2}
      />
    </TouchableOpacity>
    <TextInput
      placeholder={placeholder}
      style={styles.field}
      onChangeText={onChange}
      value={value}
    />
  </View>
)

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row'
  },
  field: {
    flex: 1,
    height: 40,
    fontSize: 12,
    paddingHorizontal: 10,
    borderRightWidth: 1,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderTopRightRadius: 3,
    borderBottomRightRadius: 3,
    borderColor: colors.secondary2
  },
  icon: {
    width: 45,
    height: 40,
    borderWidth: 1,
    borderTopLeftRadius: 3,
    borderBottomLeftRadius: 3,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: colors.secondary2
  }
})

export default Input