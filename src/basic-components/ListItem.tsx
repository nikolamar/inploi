import * as React from 'react'
import { StyleSheet, TouchableOpacity, View, Text } from 'react-native'
import Line from './Line'
import colors from '../colors'
const { MaterialIcons } = require('@expo/vector-icons')

interface Props {
  leftAvatar?: object
  rightArrow?: boolean
  title: string
  subtitle?: string
  onPress?: () => void
}

const ListItem = ({leftAvatar, rightArrow, title, subtitle, onPress}: Props) => (
  <TouchableOpacity onPress={onPress}>
    <View style={styles.container}> 
      <View style={styles.container}>
        <View style={styles.avatar}>
          {leftAvatar}
        </View>
        <View>
          {title && <Text style={styles.title}>{title}</Text>}
          {subtitle && <Text style={styles.subtitle}>{subtitle}</Text>}
        </View>
      </View>
      {rightArrow && <MaterialIcons
        name="keyboard-arrow-right"
        size={35}
        color={colors.secondary2}
      />}
    </View>
    <Line/>
  </TouchableOpacity>
)

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    height: 70,
    alignItems: "center",
    justifyContent: "space-between"
  },
  avatar: {
    marginRight: 20,
    width: 38,
    height: 38,
  },
  title: {
    fontSize: 12,
    fontFamily: "AvenirNext-Regular",
    fontWeight: "600"
  },
  subtitle: {
    fontSize: 12,
    fontFamily: "AvenirNext-Regular",
  }
})

export default ListItem
