import * as React from 'react'
import { StyleSheet, TouchableOpacity, View, Text } from 'react-native'
import Input from './Input'
import Line from './Line'
import colors from '../colors'
import { Navigation } from '../models'

interface Props {
  style?: object,
  search?: boolean,
  navigation: Navigation
}

const ClassicTabs = ({style, search, navigation}: Props) => {
  let tabs = []
  for (let i = 0; i < navigation.navigationState.routes.length; i++) {
    const selected = i === navigation.navigationState.index
    tabs.push(
      <TouchableOpacity key={i} style={{flex: 1}} onPress={() => navigation.jumpToIndex(i)}>
        <View style={styles.labelContainer}>
          <Text style={{
            opacity: selected ? 1 : 0.7,
            color: selected ? colors.primary : colors.secondary
          }}>
            {navigation.navigationState.routes[i].key.toUpperCase()}
          </Text>
        </View>
        <Line style={{
          height: selected ? 2 : 1,
          backgroundColor: selected ? colors.primary : colors.secondary,
          opacity: selected ? 1 : 0.3 
        }}/>
      </TouchableOpacity>
    )
  }
  const searchInput = <Input
    style={styles.input}
    placeholder='SEARCH JOBS'
  />
  return (
    <View style={style}>
      {search && searchInput}
      <View style={styles.container}>
        {tabs}
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
  },
  labelContainer: {
    width: "100%",
    height: 50,
    alignItems: "center",
    justifyContent: "center"
  },
  input: {
    marginHorizontal: 20
  }
})

export default ClassicTabs