import * as React from 'react'
import { StyleSheet, View } from 'react-native'
import colors from '../colors'

interface Props {
  style?: object
}

const Line = ({style}: Props) => (
  <View style={[styles.line, style]}/>
)

const styles = StyleSheet.create({
  line: {
    height: 1,
    backgroundColor: colors.secondary2
  }
})

export default Line