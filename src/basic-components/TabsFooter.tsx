import * as React from 'react'
import { StyleSheet, TouchableOpacity, View } from 'react-native'
import { Circle, Line } from '../basic-components'
import colors from '../colors'
import { Navigation } from '../models'
import { Route } from '../enums'
const { MaterialIcons } = require('@expo/vector-icons')
const callOnceInInterval = require('call-once-in-interval')

interface Props {
  navigation: Navigation
}

const createCircles = (navigation: Navigation) => {
  let view = []
  for (let i = 0; i < navigation.navigationState.routes.length; i++) {
    view.push(<Circle key={i} active={i === navigation.navigationState.index ? true : false}/>)
  }
  return view
}

const buttons = ({navigation, navigationState, jumpToIndex}: Navigation) => {
  if (navigationState.index === navigationState.routes.length-1) return (
    <TouchableOpacity onPress={callOnceInInterval(() => navigation.navigate(Route.Companies))}>
      <MaterialIcons
        name='check-circle'
        size={35}
        color={colors.primary}
      />
    </TouchableOpacity>
  )
  return (
    <TouchableOpacity onPress={() => jumpToIndex(navigationState.index+1)}>
      <MaterialIcons
        name='arrow-forward'
        size={35}
        color={colors.primary}
      />
    </TouchableOpacity>
  )
}

const TabsFooter = ({navigation}: Props) => (
  <View style={styles.container}>
    <Line/>
    <View style={styles.horizontal}>
      <View style={styles.circles}>
        {createCircles(navigation)}
      </View>
      {buttons(navigation)}
    </View>
  </View>
)

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 20,
    backgroundColor: colors.content
  },
  horizontal: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row'
  },
  circles: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 55
  }
})

export default TabsFooter