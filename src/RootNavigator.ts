import { StackNavigator } from 'react-navigation'
import Registration from './components/registration'
import SpecialityCandidate from './components/speciality-candidate'
import EmployerWalkthrough from './components/employer-walkthrough'
import CandidateWalkthrough from './components/candidate-walkthrough'
import colors from './colors'

const RootNavigator = StackNavigator(
  {
    Registration,
    SpecialityCandidate,
    CandidateWalkthrough,
    EmployerWalkthrough
  },
  {
    headerMode: 'none',
    cardStyle: {
      backgroundColor: colors.content
    }
  }
)

export default RootNavigator