interface Job {
  title: string
  subtitle: string
}

export default Job