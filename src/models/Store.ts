import Job from './Job'
import Person from './Person'

interface Store {
  fake: {
    speciality: {
      jobsList: Array<Job>,
      peopleList: Array<Person>
    }
  }
}

export default Store