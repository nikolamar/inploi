interface Job {
  wage: string
  description: string
  title: string
  company: string
}

export default Job