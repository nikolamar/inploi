interface Event {
  nativeEvent: {
    layout: {
      width: number
    }
  }
}

export default Event