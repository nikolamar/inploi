interface Navigation {
  jumpToIndex: (i: number) => void
  navigate: (name: string) => void
  navigationState: {
    routes: {
      length: number
    },
    index: number
  }
  navigation: {
    navigate: (name: string) => void
  }
}

export default Navigation