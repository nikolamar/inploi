export default {
  primary: '#E75057',
  primary2: '#00A1FF',
  primary3: '#47CB65',
  primary4: '#113A52',
  primary5: '#0083BE',
  primary6: '#3B5998',
  secondary: '#555555',
  secondary2: '#D8D8D8',
  title: '#FFFFFF',
  content: '#FFFFFF'
}