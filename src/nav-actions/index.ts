import { NavigationActions } from 'react-navigation'

export const resetAction = (routeName: string) => NavigationActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName })]
})