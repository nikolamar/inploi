import { AsyncStorage } from 'react-native'
import { autorunAsync, toJS, extendObservable } from 'mobx'

export const saveStore = (store: object, name: string) => {
  let firstRun = true
  autorunAsync(() => {
    const json = JSON.stringify(toJS(store))
    if (!firstRun) {
      AsyncStorage.setItem(name, json)
    }
    firstRun = false
  }, 500)
}

export const loadStore = async (store: object, name: string) => {
  const storeCache = await AsyncStorage.getItem(name)
  if (storeCache) {
    const data = JSON.parse(storeCache)
    extendObservable(store, data)
  }
}