import { action, observable, useStrict } from 'mobx'
import { AsyncStorage } from 'react-native'
import { saveStore, loadStore } from './handleRefresh'

useStrict(true)

class Store {
  // Observables
  @observable fake = {
    "job": { "category": "Restaurant & Hotel", "pay": "£28,000 per year", "required": "No experience required", "skills": "10 required skills", "address": "26 Astwood Mews, SW7 4DE", "latitude": 51.4939899, "longitude": -0.1864914  
    },
    "speciality": {
      "jobsList": [
        { "title": "Company Name", "subtitle": "JOB TITLE" },
        { "title": "Company Name", "subtitle": "JOB TITLE" },
        { "title": "Company Name", "subtitle": "JOB TITLE" },
        { "title": "Company Name", "subtitle": "JOB TITLE" },
        { "title": "Company Name", "subtitle": "JOB TITLE" },
        { "title": "Company Name", "subtitle": "JOB TITLE" },
        { "title": "Company Name", "subtitle": "JOB TITLE" },
        { "title": "Company Name", "subtitle": "JOB TITLE" },
        { "title": "Company Name", "subtitle": "JOB TITLE" }
      ],
      "companiesList": [
        { "title": "Company Name", "subtitle": "JOB TITLE" },
        { "title": "Company Name", "subtitle": "JOB TITLE" },
        { "title": "Company Name", "subtitle": "JOB TITLE" },
        { "title": "Company Name", "subtitle": "JOB TITLE" },
        { "title": "Company Name", "subtitle": "JOB TITLE" }
      ],
      "peopleList": [
        { "title": "Name", "subtitle": "Current Title @Company" },
        { "title": "Name", "subtitle": "Current Title @Company" }
      ],
      "recomendedJobsJobs": [
        { "title": "Company Name", "company": "Job Title", "wage": "£10 P/H" },
        { "title": "Company Name", "company": "Job Title", "wage": "£28,000 P/A" },
        { "title": "Company Name", "company": "Job Title", "wage": "£15,000 P/A" },
        { "title": "Company Name", "company": "Job Title", "wage": "£107,000 P/A" },
        { "title": "Company Name", "company": "Job Title", "wage": "£17,000 P/A" }
      ],
      "recomendedCompaniesJobs": [
        { "title": "Company Name", "company": "10 Jobs - 5 Sites" },
        { "title": "Company Name", "company": "10 Jobs - 5 Sites" },
        { "title": "Company Name", "company": "10 Jobs - 5 Sites" },
        { "title": "Company Name", "company": "10 Jobs - 5 Sites" },
        { "title": "Company Name", "company": "10 Jobs - 5 Sites" }
      ],
      "featuredJobs": [
        { "title": "Feature Job", "description": "Job Title · Company Name are looking for a new member of staff to do…", "wage": "£10 P/H" },
        { "title": "Feature Job", "description": "Job Title · Company Name are looking for a new member of staff to do…", "wage": "£28,000 P/A" },
        { "title": "Feature Job", "description": "Job Title · Company Name are looking for a new member of staff to do…", "wage": "£15,000 P/A" },
        { "title": "Feature Job", "description": "Job Title · Company Name are looking for a new member of staff to do…", "wage": "£107,000 P/A" },
        { "title": "Feature Job", "description": "Job Title · Company Name are looking for a new member of staff to do…", "wage": "£17,000 P/A" },
        { "title": "Feature Job", "description": "Job Title · Company Name are looking for a new member of staff to do…", "wage": "£61,000 P/A" }
      ],
      "featuredCompanies": [
        { "title": "Feature Company", "description": "Company name are based in the heart of London and are opening two new…", "wage": "£10 P/H" },
        { "title": "Feature Company", "description": "Company name are based in the heart of London and are opening two new…", "wage": "£28,000 P/A" },
        { "title": "Feature Company", "description": "Company name are based in the heart of London and are opening two new…", "wage": "£15,000 P/A" },
        { "title": "Feature Company", "description": "Company name are based in the heart of London and are opening two new…", "wage": "£107,000 P/A" },
        { "title": "Feature Company", "description": "Company name are based in the heart of London and are opening two new…", "wage": "£17,000 P/A" },
        { "title": "Feature Company", "description": "Company name are based in the heart of London and are opening two new…", "wage": "£61,000 P/A" }
      ],
      "similarJobs": [
        { "title": "Job Title", "company": "Company Name" },
        { "title": "Job Title", "company": "Company Name" },
        { "title": "Job Title", "company": "Company Name" },
        { "title": "Job Title", "company": "Company Name" },
        { "title": "Job Title", "company": "Company Name" },
        { "title": "Job Title", "company": "Company Name" }
      ]
    }
  }

  // Actions
  @action fetchJobs = () => {}

  constructor() {
    // Handle refresh
    loadStore(this, 'store')
    saveStore(this, 'store')

    // Load
    if (!AsyncStorage.getItem('store')) {
      this.fetchJobs()
    }
  }
}

export default new Store()